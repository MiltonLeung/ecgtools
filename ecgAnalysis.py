
from scipy.signal import butter, lfilter
import numpy as np
from numpy import genfromtxt
import matplotlib.pyplot as plt
from scipy.signal import freqz
import scipy.fftpack
from scipy.signal import stft
import sys
sys.path.append('C:\_JefferyChan\Code\pythonFunction')
from detect_peaks import detect_peaks
#import sys
#sys.path.append('C:\_JefferyChan\Code\pythonFunction')
#from QRSDetectorOffline import QRSDetectorOffline
   
#####################################
# Functions
#####################################
def findMatchIndex(array, comparedContent): # Get index of data with input ID
    index = []
    for i in range (len(array)):
        if array[i] == comparedContent:
            index.append(i)
    return index

def butter_bandpass(lowcut, highcut, fs, order=5):
     nyq = 0.5 * fs
     low = lowcut / nyq
     high = highcut / nyq
     b, a = butter(order, [low, high], btype='band')
     return b, a
   
def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def fft (data, sampleFreq): # FFT
    N = len(data)
    T = 1.0 / sampleFreq
    y = scipy.fftpack.fft(data)
    yf = 2.0/N * np.abs(y[:N//2])
    xf = np.linspace(0.0, 1.0/(2.0*T), N/2)
    return xf, yf
   
#####################################
# Main
#####################################
# Initialization
ecgExtracted = {}
wristbandID = {}

# Parameter setting
fs = 500.0
lowcut = 0.05
highcut = 20.0
filterOrder = 3
nsampleMax = 200000 # max. no. of samples in ecg to be analyzed
ecgIndex = 0 # point to the column of ecg signal
winIntegrate = 3 # window size for performing intergration
winHeartRate = 5 # moving average window size for heart rate

# Wristband ID list
wristbandIDlist = ['effca86f3ccb','f6a653393c01','f8fc9a172659','d941ca65bdc6','c676747f84af','c75e7cd9e1e0']
nID = len(wristbandIDlist)

# Load and extract signal
data = genfromtxt('C:\_JefferyChan\Data\ECG\_199workspace\ecg_20180116.csv', names=['a', 'b', 'c', 'd', 'e', 'f'], dtype = None)#,skip_header=150000, max_rows=5000)
idAll = data['e'][:]
ecgAll = data['f'][:]

for i in range (nID):
    idIndex = findMatchIndex(idAll.astype(str),wristbandIDlist[i])
    if idIndex: # if ID is in the data file
        ecgExtracted[ecgIndex] = ecgAll[idIndex]
        wristbandID[ecgIndex] = wristbandIDlist[i]
        ecgIndex = ecgIndex + 1

for i in range(ecgIndex):
    plt.figure(i+1)
    plt.suptitle('ID: ' + wristbandID[i])
    
    ecg = ecgExtracted[i][0:nsampleMax]
    nsample = len(ecg)
    T = nsample/fs
    t = np.linspace(0, T, nsample, endpoint=False)
        
    # Raw signal
    ecgMax = max(abs(ecg))
    ecgPeakIndex = detect_peaks(ecg, mph=np.std(ecg))
    
    plt.subplot(321)
    plt.plot(t, ecg)
    plt.plot(t[ecgPeakIndex], ecg[ecgPeakIndex], 'ro')
    axes = plt.gca()
    axes.set_ylim([-ecgMax,ecgMax])
    plt.xlabel('Time (sec)')
    plt.ylabel('Amplitude (mV)')
    plt.title('Raw ECG')
    
    # Filtering
    ecgFiltered = butter_bandpass_filter(ecg, lowcut, highcut, fs, order=filterOrder) # filtering
    ecgFilteredPeakIndex = detect_peaks(ecgFiltered, mph=np.std(ecgFiltered))
    
    plt.subplot(322)
    plt.plot(t, ecgFiltered)
    plt.plot(t[ecgFilteredPeakIndex], ecgFiltered[ecgFilteredPeakIndex], 'ro')
    axes = plt.gca()
    axes.set_ylim([-ecgMax,ecgMax])
    plt.xlabel('Time (sec)')
    plt.ylabel('Amplitude (mV)')
    plt.title('Filtered ECG')
    
    # FFT - raw signal
    fftxECG, fftyECG = fft (ecg, fs) # fft
    fftyMax = max(fftyECG)
    
    plt.subplot(323)
    plt.plot(fftxECG, fftyECG)
    axes = plt.gca()
    axes.set_ylim([0,fftyMax])
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude (mV)')
    
    # FFT - filtered signal
    fftxFiltered, fftyRFiltered = fft (ecgFiltered, fs) # fft
    
    plt.subplot(324)
    plt.plot(fftxFiltered, fftyRFiltered)
    axes = plt.gca()
    axes.set_ylim([0,fftyMax])
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude (mV)')
    
    # Tompkins algorithm - raw signal
    ecgDiff = np.ediff1d(np.append(ecg,ecg[-1])) # differences between consecutive elements of an array
    ecgSqr = ecgDiff **2
    ecgIntegrated = np.convolve(ecgSqr, np.ones(winIntegrate), 'same')
    ecgIntegratedPeakIndexStd1 = detect_peaks(ecgIntegrated, mph=np.std(ecgIntegrated))
    ecgIntegratedPeakIndexStd2 = detect_peaks(ecgIntegrated, mph=np.std(ecgIntegrated)*2)
    ecgIntegratedPeakIndexStd3 = detect_peaks(ecgIntegrated, mph=np.std(ecgIntegrated)*3)
    HeartRate1 = (fs*60)/ np.ediff1d(ecgIntegratedPeakIndexStd1) # heartrate based on threshold std 1
    HeartRate2 = (fs*60)/ np.ediff1d(ecgIntegratedPeakIndexStd2) # heartrate based on threshold std 2
    HeartRate3 = (fs*60)/ np.ediff1d(ecgIntegratedPeakIndexStd3) # heartrate based on threshold std 3
    aveHeartRate1 = np.convolve(HeartRate1, np.ones(winHeartRate), 'same')/ winHeartRate # moving average
    aveHeartRate2 = np.convolve(HeartRate2, np.ones(winHeartRate), 'same')/ winHeartRate # moving average
    aveHeartRate3 = np.convolve(HeartRate3, np.ones(winHeartRate), 'same')/ winHeartRate # moving average
    
    plt.subplot(325)
    plt.plot(t, ecgIntegrated)
    plt.plot(t[ecgIntegratedPeakIndexStd1], ecgIntegrated[ecgIntegratedPeakIndexStd1], 'ro',markersize=10)
    plt.plot(t[ecgIntegratedPeakIndexStd2], ecgIntegrated[ecgIntegratedPeakIndexStd2], 'yo',markersize=8)
    plt.plot(t[ecgIntegratedPeakIndexStd3], ecgIntegrated[ecgIntegratedPeakIndexStd3], 'go',markersize=4)
    plt.xlabel('Time (sec)')
    plt.ylabel('Amplitude-Tompkins (mV)')
    plt2 = plt.twinx()
    plt2.plot(t[ecgIntegratedPeakIndexStd1], np.append(aveHeartRate1,aveHeartRate1[-1]), 'r--')
    plt2.plot(t[ecgIntegratedPeakIndexStd2], np.append(aveHeartRate2,aveHeartRate2[-1]), 'y--')
    plt2.plot(t[ecgIntegratedPeakIndexStd3], np.append(aveHeartRate3,aveHeartRate3[-1]), 'g--')
    plt.ylabel('Ave. Heart Rate (bpm)')
    axes = plt.gca()
    axes.set_ylim([0,200])
    
    # Modified Tompkins algorithm - filtered signal
    ecgDiff = np.ediff1d(np.append(ecgFiltered,ecgFiltered[-1])) # differences between consecutive elements of an array
    ecgSqr = ecgDiff **2
    ecgIntegrated = np.convolve(ecgSqr, np.ones(winIntegrate), 'same')
    ecgIntegratedPeakIndexStd1 = detect_peaks(ecgIntegrated, mph=np.std(ecgIntegrated))
    ecgIntegratedPeakIndexStd2 = detect_peaks(ecgIntegrated, mph=np.std(ecgIntegrated)*2)
    ecgIntegratedPeakIndexStd3 = detect_peaks(ecgIntegrated, mph=np.std(ecgIntegrated)*3)
    HeartRate1 = (fs*60)/ np.ediff1d(ecgIntegratedPeakIndexStd1) # heartrate based on threshold std 1
    HeartRate2 = (fs*60)/ np.ediff1d(ecgIntegratedPeakIndexStd2) # heartrate based on threshold std 2
    HeartRate3 = (fs*60)/ np.ediff1d(ecgIntegratedPeakIndexStd3) # heartrate based on threshold std 3
    aveHeartRate1 = np.convolve(HeartRate1, np.ones(winHeartRate), 'same')/ winHeartRate # moving average
    aveHeartRate2 = np.convolve(HeartRate2, np.ones(winHeartRate), 'same')/ winHeartRate # moving average
    aveHeartRate3 = np.convolve(HeartRate3, np.ones(winHeartRate), 'same')/ winHeartRate # moving average
        
    plt.subplot(326)
    plt.plot(t, ecgIntegrated)
    plt.plot(t[ecgIntegratedPeakIndexStd1], ecgIntegrated[ecgIntegratedPeakIndexStd1], 'ro',markersize=10)
    plt.plot(t[ecgIntegratedPeakIndexStd2], ecgIntegrated[ecgIntegratedPeakIndexStd2], 'yo',markersize=8)
    plt.plot(t[ecgIntegratedPeakIndexStd3], ecgIntegrated[ecgIntegratedPeakIndexStd3], 'go',markersize=4)
    plt.xlabel('Time (sec)')
    plt.ylabel('Amplitude-Tompkins (mV)')
    plt2 = plt.twinx()
    plt2.plot(t[ecgIntegratedPeakIndexStd1], np.append(aveHeartRate1,aveHeartRate1[-1]), 'r--')
    plt2.plot(t[ecgIntegratedPeakIndexStd2], np.append(aveHeartRate2,aveHeartRate2[-1]), 'y--')
    plt2.plot(t[ecgIntegratedPeakIndexStd3], np.append(aveHeartRate3,aveHeartRate3[-1]), 'g--')
    plt.ylabel('Ave. Heart Rate (bpm)')
    axes = plt.gca()
    axes.set_ylim([0,200])
    
    
    
#    # STFT - raw signal
#    stftyECG, stftxECG, stftECG = stft(ecg, fs, nperseg=50, noverlap=49) # stft
#    
#    plt.subplot(325)
#    plt.pcolormesh(stftxECG, stftyECG, np.abs(stftECG)) #, vmin=0, vmax=2)
#    plt.xlabel('Time (sec)')
#    plt.ylabel('Frequency (Hz)')
#    
#    
#    # STFT - filtered signal
#    stftyFiltered, stftxFiltered, stftFiltered = stft(ecgFiltered, fs, nperseg=50, noverlap=49) # stft
#    
#    plt.subplot(326)
#    plt.pcolormesh(stftxFiltered, stftyFiltered, np.abs(stftFiltered)) #, vmin=0, vmax=2)
#    plt.xlabel('Time (sec)')
#    plt.ylabel('Frequency (Hz)')
    
           
    plt.show()